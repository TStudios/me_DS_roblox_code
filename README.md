# Roblox Code

Some Open-Source Code by me_DS (me)

## Licensing

Most of the code is under the AGPL3.0
Code that doesn't have a specific language specified at the top of the document is licensed under the AGPL3.0
Some of my code may have a specific license. See top of the document for info.
Make sure to credit my code!

P.S. To be able to legally use my code, you must have [this](https://gitlab.com/TStudios/me_DS_roblox_code/blob/master/minimum_credits.lua) at the begining of ***ALL*** of my scripts

(potentially outdated version below)
```lua
--[[
    Script by me_DS
    Give credit to this script, if it is used **anywhere**
    Source: https://gitlab.com/TStudios/me_DS_roblox_code
    Removal of the print functions and/or of this comment may result in legal charges
]]--
local seperator = "----------------------------------------------------------"
print(seperator)
print("An Open Source Script by me_DS has been loaded.")
print("Removal of these print lines may result in legal charges.")
print("Thanks for using my script :D")
print(seperator)
```

## Issues

If some code breaks, file an issue. Make sure to include the name of the file in the name of the issue (and optionally in the contents of the issue too)
