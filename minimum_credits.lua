--[[
    Script by me_DS
    Give credit to this script, if it is used **anywhere**
    Source: https://gitlab.com/TStudios/me_DS_roblox_code
    Removal of the print functions and/or of this comment may result in legal charges - same counts for editing them.
]]--
local seperator = "----------------------------------------------------------"
print(seperator)
print("An Open Source Script by me_DS has been loaded.")
print("Removal of these print lines may result in legal charges.")
print("Thanks for using my script :D")
print(seperator)